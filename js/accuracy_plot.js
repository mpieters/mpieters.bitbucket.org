function Plot_graph_accuracy(plotInterval, name){
  var graphDiv = document.getElementById('results');
  var plotInterval = plotInterval;
	
  var trace_accuracy = {x:[], 
						y:[],
						type: 'scatter',
						name: 'accuracy'};
					
  var trace_loss = {x:[], 
					y:[],
					type: 'scatter',
					name: 'loss'};
										
  var data = [trace_accuracy, trace_loss];
					
  var layout = {title: 'Accuracy + Loss for ' + name};
  Plotly.newPlot(graphDiv, data, layout, {displayModeBar:false});
	
  this.replot = function(step, accuracy, loss){	
    trace_accuracy.x.push(step);
	trace_accuracy.y.push((accuracy/plotInterval)*100);
	trace_loss.x.push(step);
	trace_loss.y.push((loss/plotInterval)*100);
	Plotly.newPlot(graphDiv, data, layout, {displayModeBar:false});	
  }
}	