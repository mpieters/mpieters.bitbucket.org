function Plot_graph_contour(plotInterval, name){
  var surfaceDiv = document.getElementById('contour');
  var plotInterval = plotInterval;
  var surface_layout = {title: "Contour plot of " + name};
  var contour_step = 20;
  this.replot = function(step, network){

    var z_data = [];
    var x_y_data = [];
    for(var i=0; i< contour_step ; i ++){
      var row = [];
	  for(var j=0; j< contour_step; j++){
	    row.push(0 + math.round(network.forward([i/contour_step,j/contour_step]),3));
	  }	
	  z_data.push(row);
	  x_y_data.push(i/contour_step);
    }
    var data_z1 = {z: z_data, x: x_y_data, y:x_y_data, type: 'contour'};
    Plotly.newPlot(surfaceDiv, [data_z1], surface_layout, {displayModeBar:false});	
  }
}