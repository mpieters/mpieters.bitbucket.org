function sigmoid(t){
	if (t >= 0){
		var z = Math.pow(Math.E, -t);
		return 1 / (1 + z);
	}else{
		var z = Math.pow(Math.E, t);
		return z/(1+ z);
	}
}

function write_to_log(text){
	document.getElementById('log').innerHTML += text + '\n\n';
}

function write_to_results(value1, value2){
	var table = document.getElementById("results");

	// Create an empty <tr> element and add it to the 1st position of the table:
	var row = table.insertRow(-1);
	
	// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	
	// Add some text to the new cells:
	cell1.innerHTML = value1;
	cell2.innerHTML = value2;
}

function dt_sigmoid(t){
	return t * (1 - t);
}

function sigmoid_layer(layer){
	var l = layer.length;
	var copy = math.zeros([l,1]);
	for (var i=0; i < l; i++){
    	copy[i] =  sigmoid(layer[i]);
    }
    return copy;
}

function dt_sigmoid_layer(layer){
	var l = layer.length;
	var copy = math.zeros([l,1]);
	for (var i=0; i < l; i++){
    	copy[i] = dt_sigmoid(layer[i]);
    }
    return copy;
}

function Layer(number_of_nodes){
	this.nodes = number_of_nodes;
}

function Network(input_layer, hidden_layers, output_layer, learning_rate, dist, task){
	var network = this;
	
	
	var dist = dist;
	var lr = learning_rate;
	this.nr_forward_passes = 0;
	this.loss = 0;
	
	var task = task;
	
	var output0, output1, output2;
	var target, correct;
	
	var layer1 = math.random([input_layer.nodes, hidden_layers.nodes], -dist, dist);
	
	var layer2 = math.random([hidden_layers.nodes, output_layer.nodes], -dist, dist);
	

	function forward_pass(input){
		output0 = input;
		output1 = sigmoid_layer(math.multiply(output0, layer1));
		output2 = sigmoid_layer(math.multiply(output1, layer2));
	}
	
	
	function calculate_accuracy(t){
		if(task === "binary"){
			target = t;
			network.loss = math.abs(target - output2);
			network.correct = math.deepEqual(target, math.round(output2)) ? 1 : 0;
		}else if(task == "one_hot"){
			target = t;
			var max = -10000;
			var index = -1;
			for (var i=0; i<output_layer.nodes; i++){
				if(output2[i] > max){
					max = output2[i];
					index = i;
				}
			}
			network.correct = (target[index] == 1) ? 1 : 0;
			
			network.loss = 0;
			for (var i=0; i<output_layer.nodes; i++){
				network.loss += (target[i] - output2[i])*(target[i] - output2[i]);
			}
			network.loss = 0.5 * network.loss;
		}
	}
	
	function perform_backprop(){
		
			
			var error = math.subtract(output2, target);
			
			
			var dt_output1 = dt_sigmoid_layer(output1);
			var dt_output2 = dt_sigmoid_layer(output2);
			
			var l2delta = math.dotMultiply(error, dt_output2);
			
			var output0_t = math.transpose([output0]);
			var output1_t = math.transpose([output1]);
			
			var layer2_t = math.transpose(layer2);
			
			var l1error = math.multiply(l2delta, layer2_t);
			
			
			var l1delta = math.dotMultiply(l1error, dt_output1);


			var layer1_step = math.multiply(output0_t, [l1delta]);
			var layer2_step = math.multiply(output1_t, [l2delta]);
			var lr_layer1 = math.multiply(lr, layer1_step);
			var lr_layer2 = math.multiply(lr, layer2_step);
			
			layer1 = math.subtract(layer1,  lr_layer1);
			layer2 = math.subtract(layer2,  lr_layer2);
					
	}
		
		
	
	this.iteration = function(i, t){
		this.nr_forward_passes += 1;
		forward_pass(i);
		calculate_accuracy(t);
		perform_backprop();
	}
	
	this.forward = function(i){
		forward_pass(i);
		return output2;
	}
	
}

