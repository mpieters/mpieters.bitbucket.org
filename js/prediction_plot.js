function getPixel(imgData, index) {
  var i = index*4, d = imgData.data;
  return [d[i],d[i+1],d[i+2],d[i+3]] // returns array [R,G,B,A]
}

// AND/OR

function getPixelXY(imgData, x, y) {
  return getPixel(imgData, y*imgData.width+x);
}


function Plot_graph_prediction(){
  var predictionDiv = document.getElementById('prediction');
  
  var layout = {title: 'Prediction for image'};
  
  var data = [{x:[0,1,2,3,4,5,6,7,8,9], 
			   y:[],
					type: 'bar'}];
  
  Plotly.newPlot(predictionDiv, data, layout , {displayModeBar:false});
  
  var canvas = document.getElementById("canvas");
  var context = canvas.getContext("2d");
  
  
  
  this.replot = function(network){
	  
	  var data = context.getImageData(0, 0, canvas.width, canvas.height);
	  
  
  
  
  			   var test_input = [];
			   for(var i=0; i<280; i+=10){
				   for(var j=0; j<280; j+=10){
					   
					   var pixel = getPixelXY(data, i, j);
					   
					   var color = pixel[3];
					   
					   test_input.push(color/255);
					   
				   }
			   }
			   
			   var output = network.forward(test_input);
			   
			   var exp_sum = 0;
			   for(var i=0; i<output.length; i++){
				   exp_sum += Math.pow(Math.E, output[i]);
			   }
			   for(var i=0; i<output.length; i++){
				   output[i] = Math.pow(Math.E, output[i])/exp_sum;
			   }
			   
			      var data = [{x:[0,1,2,3,4,5,6,7,8,9], 
					y:output,
					type: 'bar'}];
				
				Plotly.newPlot(predictionDiv, data, layout , {displayModeBar:false});
  
  }
  

}